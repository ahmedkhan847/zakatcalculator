var category = "";
var n1,
  n2,
  n3 = "";
var zakat = "";
var to,
  from = "";

$(document).ready(function () {
  $("#submit").click(function () {
    event.preventDefault();
  });
  $(".close_btn").click(function () {
    $(".main_popup").hide();
  });
  $(".icon-box").on("click touchstart", function () {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
    } else {
      $(".icon-box").removeClass("active");
      $(this).addClass("active");
    }
    $("#main_form").show();
    $(".select-box").addClass("xs-hide");
  });
  $("#submit").on("click touchstart", function () {
    calculateZakat();
  });
  $(".err").click(function () {
    $(".main_popup").toggle();
  });

  $(".row1").click(function () {
    $("#fourth_field").show();
  });
  $(".row2").click(function () {
    $("#third_field").show();
  });
  $(".row3").click(function () {
    $("#fourth_field").show();
  });
  $(".row4").change(function () {
    if ($(this).val() == "") {
      $(this).attr("placeholder", "Fill this field");
    } else {
      // n1 = parseInt($("#four").val());
      if (parseInt($("#four").val()) > 5) {
        $("#fifth_field").show();
      } else {
        if (parseInt($("#four").val()) != 0) {
          $(".main_popup").show();
        }
      }
    }

  });
  $(".row5").change(function () {
    if ($(this).val() == "") {
      $(this).attr("placeholder", "Fill this field");
    } else {
      // n1 = parseInt($("#five").val());
      if (parseInt($("#five").val()) >= 5 && parseInt($("#five").val()) <= parseInt($("#four").val())) {
        $("#sixth_field").show();
      } else {

        $(".main_popup").show();

      }
    }
  });
  $(".row5").blur(function () {
    if ($(this).val() == "") {
      $(this).attr("placeholder", "Fill this field");
    } else {
      // n1 = parseInt($("#five").val());
      if (parseInt($("#five").val()) >= 5 && parseInt($("#five").val()) != 0 && parseInt($("#five").val()) <= parseInt($("#four").val())) {
        $("#sixth_field").show();
      } else {

        $(".main_popup").show();

      }
    }
  });
  $(".row6").change(function () {
    if ($(this).val() == "") {
      $(this).attr("placeholder", "Fill this field");
    } else {
      n2 = parseInt($("#six").val());
      // var result = n2 - n1;
      if (n2 >= 5 && n2 <= parseInt($("#five").val())) {
        // $("#seven").val(result);
        // $("#zakat").html(result);
        $("#seventh_field").show();
        $("#finalZakat").css("display", "");
      } else {

        $(".main_popup").show();

      }
    }
  });
  $(".row6").blur(function () {
    if ($(this).val() == "") {
      $(this).attr("placeholder", "Fill this field");
    } else {
      n2 = parseInt($("#six").val());
      var result = n2 - n1;
      if (n2 >= 5 && n2 <= parseInt($("#five").val())) {
        $("#seventh_field").show();
      } else {

        $(".main_popup").show();

      }
    }
  });
  $(".row7").change(function () {
    if ($(this).val() == "") {
      $(this).attr("placeholder", "Fill this field");
    } else {
      n1 = parseInt($("#seven").val());
      $("#twelve_field").show();
    }
  });
  $(".row7").blur(function () {
    if ($(this).val() == "") {
      $(this).attr("placeholder", "Fill this field");
    } else {
      n1 = parseInt($("#seven").val());
      $("#twelve_field").show();
    }
  });
  // $(".row8").click(function(){
  // 	$("#ninth_field").show();
  // });

  // $('#to').blur(function() {
  //     if ($(this).val() == "") {
  //         $(this).attr("placeholder", "Fill this field");
  //     }
  //     else {
  //         var from = $('#from').val();
  //         var to = $('#to').val();
  //         console.log(from);
  //         console.log(to);
  //         parsefrom = parseDate(from);
  //         parseto = parseDate(to);
  //         console.log(daydiff(parsefrom, parseto));
  //         $("#ninth_field").show();
  //     }
  // });

  $("#to").change(function () {
    if ($(this).val() == "") {
      $(this).attr("placeholder", "Fill this field");
    } else {
      var from = $("#from").val();
      var to = $("#to").val();
      parsefrom = parseDate(from);
      parseto = parseDate(to);
      datediff = daydiff(parsefrom, parseto);
      if (datediff >= 366 || datediff >= 365) {
        $("#ninth_field").show();
      } else {
        if (datediff != 0) {
          $(".main_popup").show();
        }
      }
    }
  });

  $(".row9").click(function () {
    $("#tenth_field").show();
  });

  $(".row10").click(function () {
    $("#eleventh_field").show();
  });

  $(".row11").click(function () {
    $("#twelve_field").show();
  });

  $(".in_img").click(function () {
    $(".row8").click();
  });
});

function calculateZakat() {
  var result = n2 - n1;
  if (result < 5) {
    $(".main_popup").show();
  }
  var zakat = "";
  var category = $(".active > p").text();
  // console.log(category);
  switch (category) {
    case "CATTLE":
      console.log(category);
      console.log(result);
      if (result < 10) {
        zakat = "1 sheep";
        console.log("here1");
      } else if (result < 15) {
        zakat = "2 sheeps";
      } else if (result < 20) {
        zakat = "3 sheeps";
      } else if (result < 25) {
        zakat = "4 sheeps";
      } else if (result < 36) {
        zakat =
          "1bint makhaz (in 2nd year) (BABY CAMEL) </br> Ibne laboon (in 3rd year) (3 YEAR OLD CAMEL) </br> Buy what ever you want";
      } else if (result < 46) {
        zakat = "Bint laboon (in 3rd year)";
      } else if (result < 61) {
        zakat = "Hiqqa (in 4th year)(CAMEL OF 4 YRS)";
      } else if (result < 76) {
        zakat = "Jiza (in 5th)";
      } else if (result < 91) {
        zakat = "2 bint laboon (in 3rd year)";
      } else if (result < 121) {
        zakat = "2 Hiqqa";
      } else if (result > 121) {
        if (result % 40 == 0 && result % 50 == 0) {
          var x = result / 40;
          var y = result / 50;
          console.log("divisible by both");
          console.log(result % 40);
          console.log(result % 50);
          zakat = x + " bint laboon or " + y + " hiqqa";
        } else if (result % 40 == 0 && result % 50 != 0) {
          var x = result / 40;
          zakat = x + " bint laboon";
          console.log("divisible by 40");
          console.log(result % 40);
        } else if (result % 40 != 0 && result % 50 == 0) {
          var y = result / 50;
          zakat = y + " hiqqa";
          console.log("divisible by 50");
          console.log(result % 50);
        } else {

          var x = result / 40;
          var y = result / 50;
          var range = x / 2 > y / 2 ? Math.round(x / 2) : Math.round(y / 2);
          var nextZakat = true;
          var zakats = [];


          if (result > 880) {
            mainRange = 30;
          }


          if (result > 1600) {
            mainRange = 30;
          }

          var xi = 0;
          var xj = 20;
          var yi = Math.round(y) - 20;
          var yj = Math.round(y) + 20;
          /* console.log(range) */
          /* console.log(i); */
          /* console.log(j); */
          while (xi <= xj) {
            var k = yi;
            while (k <= yj) {
              if (xi > 0 && k > 0) {
                var x = 40 * xi;
                var y = 50 * k;
                var currentValue = result - (x + y);
                console.log("Zakat 1");
                console.log(currentValue);
                if (currentValue <= 9) {
                  if (currentValue > 0) {
                    zakats.push(xi + " bint laboon and " + k + " hiqqas");
                  }

                  if (currentValue == 0) {
                    nextZakat = false;
                    zakats[0] = xi + " bint laboon and " + k + " hiqqas";
                    i = j;
                    k = j;
                  }
                } 
              }

              k++;
            }
            xi++;
          }
          console.log(zakats);
          var zakat2 = [];
          x = result / 40;
          /* console.log(x);// */
          yi = 0;
          yj = 20;
          xi = Math.round(x) - 20;
          xj = Math.round(x) + 20;
          /* console.log(xi); */
          /* console.log(xj); */
          while (yi <= yj) {
            var k = xi;
            while (k <= xj) {
              if (yi > 0 && k > 0) {
                var x = 40 * k;
                var y = 50 * yi;
                var currentValue = result - (x + y);
                console.log("Zakat 2");
                console.log(currentValue) 
                ;
                if (currentValue <= 9) {
                  if (currentValue > 0) {
                    zakat2.push(yi + " hiqqas and " +k + " bint laboon");
                  }

                  if (currentValue == 0) {
                    zakat2[0] = k + " bint laboon and " + yi + " hiqqas";
                    yi = yj;
                    k = xj;
                  }
                } 
              }

              k++;
            }
            yi++;
          }
          console.log(zakat2);
          if(zakat2.length > 0 && zakats.length > 0){
            zakat = "<br>" + zakats[0] + "<br><b>OR</b><br>" + zakat2[0];
          }else{
            if(zakat2[0] == zakats[0]){
              zakat = zakats[0];
            }else{
              zakat = zakat2[0];
            }
            
          }
          
        }
      }
      break;
    default:
      console.log("default");
      if (result < 10) {
        zakat = "1 sheep";
      } else if (result < 15) {
        zakat = "2 sheeps";
      } else if (result < 20) {
        zakat = "3 sheeps";
      } else if (result < 25) {
        zakat = "4 sheeps";
      } else if (result < 36) {
        zakat =
          "1bint makhaz (in 2nd year) (BABY CAMEL) </br> Ibne laboon (in 3rd year) (3 YEAR OLD CAMEL) </br> Buy what ever you want";
      } else if (result < 46) {
        zakat = "Bint laboon (in 3rd year)";
      } else if (result < 61) {
        zakat = "Hiqqa (in 4th year)(CAMEL OF 4 YRS)";
      } else if (result < 76) {
        zakat = "Jiza (in 5th)";
      } else if (result < 91) {
        zakat = "2 bint laboon (in 3rd year)";
      } else if (result < 121) {
        zakat = "2 Hiqqa";
      } else if (result >= 121) {
        console.log(result % 40);
        console.log(result % 50);

        if (result % 40 == 0 && result % 50 == 0) {
          var x = result / 40;
          var y = result / 50;
          zakat = x + " bint laboon or " + y + " hiqqa";
        } else if (result % 40 == 0 && result % 50 != 0) {
          var x = result / 40;
          zakat = x + " bint laboon";
        } else if (result % 40 != 0 && result % 50 == 0) {
          var y = result / 50;
          zakat = y + " hiqqa";
          if (result % 40 == 0 && result % 50 == 0) {
            var x = result / 40;
            var y = result / 50;
            zakat = x + " bint laboon or " + y + " hiqqa";
          }
        } else {
          var mainRange = 10;
          var x = result / 40;
          var y = result / 50;
          var range = x / 2 > y / 2 ? Math.round(x / 2) : Math.round(y / 2);
          var previousValue = 0;
          var zakats = [];

          if (result > 800) {
            mainRange = 20;
          }

          if (result > 1600) {
            mainRange = 30;
          }

          var i = range - mainRange;
          var j = range + mainRange;

          while (i <= j) {
            var k = range - mainRange;
            while (k <= j) {
              if (i > 0 && k > 0) {
                var x = 40 * i;
                var y = 50 * k;
                var currentValue = result - (x + y);

                if (currentValue <= 9) {
                  if (currentValue > 0) {
                    zakats.push(i + " bint laboon and " + k + " hiqqas");
                  }

                  if (currentValue == 0) {
                    zakats = [];
                    zakats.push(i + " bint laboon and " + k + " hiqqas");
                    i = j;
                    k = j;
                  }

                }
              }

              k++;
            }
            i++;
          }

          zakat = zakats.length != 1 ? "<br>" + zakats[0] + "<br><b>OR</b><br>" + zakats[zakats.length - 1] : zakats[0];
        }
      }
      break;
  }

  $("#wrap").css("display", "");
  $("#zakat").html(zakat);
  console.log(zakat);
}

function parseDate(str) {
  return new Date(str);
}

function daydiff(first, second) {
  //Get 1 day in milliseconds
  var one_day = 1000 * 60 * 60 * 24;

  // Convert both dates to milliseconds
  var date1_ms = first.getTime();
  var date2_ms = second.getTime();

  // Calculate the difference in milliseconds
  var difference_ms = date2_ms - date1_ms;
  // Convert back to days and return
  return Math.round(difference_ms / one_day);
}
