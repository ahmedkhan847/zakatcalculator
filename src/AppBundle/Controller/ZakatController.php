<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ZakatController extends Controller
{
    /**
     * @Route("/zakat/")
     */
    public function indexAction()
    {
        // return $this->render('AppBundle:ZakatController:index.html.twig', array(
        //     // ...
        // ));
        return $this->render('default/zakat.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

}
